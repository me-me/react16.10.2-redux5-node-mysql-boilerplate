import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Redirect,
  Route,
} from "react-router-dom";
import Home from "./components/pages/Dashboard/Home";
import Login from "./components/pages/Login/Login";
import  Logout from './components/pages/Logout/Logout';
import decode from 'jwt-decode';
import NotFound from './components/pages/NotFound/NotFound';
require('dotenv').config();

function App() {
  const checkAuth = () => {
    const token = localStorage.getItem('token');
    if (!token) {
      return false;
    }
  
    try {
      // { exp: 12903819203 }
      const { exp } = decode(token);
  
      if (exp < new Date().getTime() / 1000) {
        return false;
      }
  
    } catch (e) {
      return false;
    }
  
    return true;
  }
  
  const AuthRoute = ({ component: Component, ...rest }) => (
    <Route {...rest} render={props => (
      checkAuth() ? (
        <Component {...props} />
      ) : (
          <Redirect to={{ pathname: '/login' }} />
        )
    )} />
  )

  return (          
  <div className="row">
   <Router>
   <Switch>
          <Route exact path="/" component={Home} />
          <Route exact path="/login" component={Login} />
          <Route exact path="/logout" component={Logout} />
          <AuthRoute exact component={NotFound} />
      </Switch>
   </Router>
  </div>
  );
}

export default App;
