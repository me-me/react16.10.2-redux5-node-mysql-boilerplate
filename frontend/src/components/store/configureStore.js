import { createStore, combineReducers, compose } from 'redux';
import { ConnectedRouter as Router, routerReducer, routerMiddleware } from "react-router-redux";
import authReducer from './reducers/userReducers/auth_reducer';

const reducers = combineReducers({ 
    router: routerReducer,
    auth: authReducer,
  });

export default reducers;