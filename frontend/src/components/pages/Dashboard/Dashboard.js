
import React from "react";


class Dashboard extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
         let style = {
            width: "100%",
            height: "125px",
            fontSize: "14px",
            lineHeight: "18px", 
            border: "1px solid #dddddd", 
            padding: "10px",
        };

        let style1 = {
            marginRight: "5px"
        };

        let style2 = {
            height: "250px",
            width: "100%",
        };

        let style3 = {
            borderRight: "1px solid #f4f4f4"
        };

        let style4 = {
            height: "250px",
        };

        let style5 = {
            fontSize: "20px"
        };

        let style6 = {
            position: "relative", 
            height: "300px",
        };

        let style7 = {
            width: "100%"
        }

        
        return (
            <div>
                             <div className="content-wrapper">
     <section className="content-header">
      <h1>
          Tableau de bord
        <small>Control panel</small>
      </h1>
      <ol className="breadcrumb">
        <li><a href="#"><i className="fa fa-dashboard"></i> Home</a></li>
        <li className="active">Dashboard</li>
      </ol>
    </section>

    <section className="content">
      <div className="row">
        <div className="col-lg-3 col-xs-6">
          <div className="small-box bg-aqua">
            <div className="inner">
              <h3>150</h3>

              <p>Evenement active</p>
            </div>
            <div className="icon">
              <i className="ion ion-bag"></i>
            </div>
            <a href="#" className="small-box-footer">More info <i className="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
         <div className="col-lg-3 col-xs-6">
           <div className="small-box bg-green">
            <div className="inner">
              <h3>53<sup style={style5}>%</sup></h3>

              <p>Utilisateurs</p>
            </div>
            <div className="icon">
              <i className="ion ion-stats-bars"></i>
            </div>
            <a href="#" className="small-box-footer">More info <i className="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
         <div className="col-lg-3 col-xs-6">
           <div className="small-box bg-yellow">
            <div className="inner">
              <h3>44</h3>

              <p>Passions</p>
            </div>
            <div className="icon">
              <i className="ion ion-person-add"></i>
            </div>
            <a href="#" className="small-box-footer">More info <i className="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
         <div className="col-lg-3 col-xs-6">
           <div className="small-box bg-red">
            <div className="inner">
              <h3>65</h3>

              <p>Competences</p>
            </div>
            <div className="icon">
              <i className="ion ion-pie-graph"></i>
            </div>
            <a href="#" className="small-box-footer">More info <i className="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
       </div>

         <div class="content">

    <section class="content">
      <div class="row">
        <div class="col-md-6">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Graph des evenements</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <div class="box-body">
              <div class="chart">
                <canvas id="areaChart" style={{height:"250px"}}></canvas>
              </div>
            </div>
          </div>
          <div class="box box-danger">
            <div class="box-header with-border">
              <h3 class="box-title">Donut Chart</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <div class="box-body">
              <canvas id="pieChart" style={{height:"250px"}}></canvas>
            </div>
          </div>

        </div>
        <div class="col-md-6">
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Line Chart</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <div class="box-body">
              <div class="chart">
                <canvas id="lineChart" style={{height:"250px"}}></canvas>
              </div>
            </div>
          </div>

          <div class="box box-success">
            <div class="box-header with-border">
              <h3 class="box-title">Bar Chart</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <div class="box-body">
              <div class="chart">
                <canvas id="barChart" style={{height:"230px"}}></canvas>
              </div>
            </div>
          </div>

        </div>
      </div>

    </section>
  </div>

 
    </section>
            </div>
            </div>
        );
    }
}

export default Dashboard;