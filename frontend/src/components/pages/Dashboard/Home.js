import React from 'react';
import Header from '../../common/Header';
import SideBar from '../../common/SideBar';
import Footer from '../../common/Footer';

class Home extends React.Component {

    render() {
        return <div>
            <Header />
            <SideBar />
            <Footer />
        </div>;
    }
}


export default Home;