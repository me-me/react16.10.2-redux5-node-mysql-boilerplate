import { combineReducers } from 'redux';
import auth from './userReducers/auth_reducer';
import info from './userReducers/user_infoReducer';
import add from './userReducers/user_addReducer';
import {getUserByCom } from './userReducers/get_users_byCommunityIdReducer';
import deletedUser from './userReducers/user_deleteReducer';
import editUser from './userReducers/user_editReducer';
import userIdInfo from './userReducers/getUserPropsIdReducer';
import eventAdd from './eventReducers/EventAddReducer';
import {getEventByCom} from './eventReducers/get_eventsBy_communityId';
import eventIdInfo from './eventReducers/GetEventIdReducer';
import deletedEvent from './eventReducers/EventDeleteReducer';
import editEvent from './eventReducers/EventEditReducer';


const rootReducer = combineReducers({
    auth,
    info,
    add,
    getUserByCom,
    deletedUser,
    editUser,
    userIdInfo,
    eventAdd,
    getEventByCom,
    eventIdInfo,
    deletedEvent,
    editEvent
});


export default rootReducer;